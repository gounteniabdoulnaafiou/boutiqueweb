/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tg.unilome.cic.boutique.service;

import java.util.LinkedList;
import java.util.List;
import tg.univlome.cic.boutique.entites.Produit;

/**
 *
 * @author GOUNTENI
 */
public class ProduitService {
    
    
    private static List<Produit> liste;
    
    
    public void ajouter(Produit e){
        liste.add(e);
    }
    
    public void modifier(Produit e){
        for(Produit c :liste){
            if(c.equals(e)){
                liste.set(liste.indexOf(c), e);
            }
        }
    }
    
    
    // renvoie l'objet Categorie de la liste qui a l'id passé en paramètre
    public Produit trouver(Integer id){
        Produit trouver = null;
        for(Produit c :liste){
            if(c.getId() == id){
                trouver = c;
            }
        }
        return trouver;
    }
    
    // retirer de la liste, l'objet Categorie qui a l'id passé en paramètre
    public void supprimer(Integer id){
        if(id == null || id <= 0){
            throw new IllegalArgumentException("id null ou négatif");
        }
        
        for(Produit removable :liste){
            if(removable.getId() == id){
                liste.remove(removable);
            }
        }
    }
    
    // retirer de la liste, l'objet Categorie passé en paramètre
    public void supprimer(Produit e){
        if(liste.contains(e)){
            liste.remove(e);
        }
    }    
    
    //renvoyer tous les éléments de la liste
    public List<Produit> lister(){
        LinkedList listeTotal = new LinkedList();
        liste.forEach(c -> {
            listeTotal.add(c);
        });
        return listeTotal;
    }
    
    // renvoyer nombre éléments de la liste, commençant à la position debut
    public List<Produit> lister(int debut, int nombre){
        return liste.subList(debut, nombre);
    }
        

}
