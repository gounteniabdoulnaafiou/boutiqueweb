package tg.unilome.cic.boutique.service;


import java.util.LinkedList;
import java.util.List;
import tg.univlome.cic.boutique.entites.ProduitAchete;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author GOUNTENI
 */
public class ProduitAcheteService {
    
    
    private static List<ProduitAchete> liste;
    
    
    public void ajouter(ProduitAchete e){
        liste.add(e);
    }
    
    public void modifier(ProduitAchete e){
        for(ProduitAchete c :liste){
            if(c.equals(e)){
                liste.set(liste.indexOf(c), e);
            }
        }
    }
    
    
    // renvoie l'objet Categorie de la liste qui a l'id passé en paramètre
    /*pa possible produitachete ne possede pas d'id
    public ProduitAchete trouver(Integer id){
        ProduitAchete trouver = null;
        for(ProduitAchete c :liste){
            if(c.getId() == id){
                trouver = c;
            }
        }
        return trouver;
    }
*/
    
    // retirer de la liste, l'objet Categorie qui a l'id passé en paramètre
    /*impossible egalement pas d'id
    public void supprimer(Integer id){
        if(id == null || id <= 0){
            throw new IllegalArgumentException("id null ou négatif");
        }
        
        for(ProduitAchete removable :liste){
            if(removable.getId() == id){
                liste.remove(removable);
            }
        }
    }
    */
    
    // retirer de la liste, l'objet Categorie passé en paramètre
    public void supprimer(ProduitAchete e){
        if(liste.contains(e)){
            liste.remove(e);
        }
    }    
    
    //renvoyer tous les éléments de la liste
    public List<ProduitAchete> lister(){
        LinkedList listeTotal = new LinkedList();
        liste.forEach(c -> {
            listeTotal.add(c);
        });
        return listeTotal;
    }
    
    // renvoyer nombre éléments de la liste, commençant à la position debut
    public List<ProduitAchete> lister(int debut, int nombre){
        return liste.subList(debut, nombre);
    }
        

}
