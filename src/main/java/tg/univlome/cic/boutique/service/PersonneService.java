/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tg.univlome.cic.boutique.service;

import java.util.LinkedList;
import java.util.List;
import tg.univlome.cic.boutique.entites.Personne;

/**
 *
 * @author GOUNTENI
 */
public class PersonneService {
    protected static List<Personne> listep;

    
    public void ajouter(Personne P){
        listep.add(P);
    }
    
    public void modifier(Personne P){
        for(Personne c :listep){
            if(c.equals(P)){
                listep.set(listep.indexOf(c), P);
            }
        }
    }
    
    
    // renvoie l'objet Categorie de la liste qui a l'id passé en paramètre
    public Personne trouver(Integer id){
        Personne trouver = null;
        for(Personne P :listep){
            if(P.getId() == id){
                trouver = P;
            }
        }
        return trouver;
    }
    
    // retirer de la liste, l'objet Categorie qui a l'id passé en paramètre
    public void supprimer(Integer id){
        if(id == null || id <= 0){
            throw new IllegalArgumentException("id null ou négatif");
        }
        
        for(Personne removable :listep){
            if(removable.getId() == id){
                listep.remove(removable);
            }
        }
    }
    
    // retirer de la liste, l'objet Categorie passé en paramètre
    public void supprimer(Personne P){
        if(listep.contains(P)){
            listep.remove(P);
        }
    }    
    
    //renvoyer tous les éléments de la liste
    public List<Personne> lister(){
        LinkedList listeTotal = new LinkedList();
        listep.forEach(P -> {
            listeTotal.add(P);
        });
        return listeTotal;
    }
    
    // renvoyer nombre éléments de la liste, commençant à la position debut
    public List<Personne> lister(int debut, int nombre){
        return listep.subList(debut, nombre);
    }
 
}
