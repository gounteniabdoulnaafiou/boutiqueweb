/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tg.univlome.cic.boutique.rs;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import tg.univlome.cic.boutique.entites.Categorie;
import tg.univlome.cic.boutique.service.CategorieService;

/**
 *
 * @author GOUNTENI
 */
@Path("/categories")
public class CategorieResource {
        
    CategorieService macategorie;
        
    
    @POST
    @Path("/ajouter")
    public void ajouter(@QueryParam("c") Categorie c){
        macategorie.ajouter(c);
    }
    
    @PATCH
    @Path("/modifier{c}")
    public void modifier(@PathParam("c") Categorie c){
        macategorie.modifier(c);
    }
    
    @GET
    @Path("/trouver")
    public void trouver(@QueryParam("id") Integer id){
        macategorie.trouver(id);
    }
    
    
    @DELETE
    @Path("/delete{id}")
    public void delete(@PathParam("id") Integer id){
        macategorie.supprimer(id);
    }
    
    
    @DELETE
    @Path("/delete{c}")
    public void delete(@PathParam("c") Categorie c){
        macategorie.supprimer(c);
    }
    
    
    @GET
    @Path("/lister")
    public void afficher(){
       macategorie.lister();
   }
    
  
    @GET
    @Path("/lister")
    public void afficher(@QueryParam("d") Integer d, @QueryParam("f") Integer f){
       macategorie.lister(d, f);
   }
}
