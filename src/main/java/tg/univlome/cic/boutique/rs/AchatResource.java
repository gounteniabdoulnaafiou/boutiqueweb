/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tg.univlome.cic.boutique.rs;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import tg.univlome.cic.boutique.service.AchatService;
import tg.univlome.cic.boutique.entites.Achat;


/**
 *
 * @author GOUNTENI
 */
@Path("/achats")
public class AchatResource {
    
    
    AchatService monachat;
        
    
    @POST
    @Path("/ajouter")
    public void ajouter(@QueryParam("c") Achat c){
        monachat.ajouter(c);
    }
    
    @PATCH
    @Path("/modifier{c}")
    public void modifier(@PathParam("c") Achat c){
        monachat.modifier(c);
    }
    
    @GET
    @Path("/trouver")
    public void trouver(@QueryParam("id") Integer id){
        monachat.trouver(id);
    }
    
    
    @DELETE
    @Path("/delete{id}")
    public void delete(@PathParam("id") Integer id){
        monachat.supprimer(id);
    }
    
    
    @DELETE
    @Path("/delete{c}")
    public void delete(@PathParam("c") Achat c){
        monachat.supprimer(c);
    }
    
    
    @GET
    @Path("/lister")
    public void afficher(){
       monachat.lister();
   }
    
  
    @GET
    @Path("/lister")
    public void afficher(@QueryParam("d") Integer d, @QueryParam("f") Integer f){
       monachat.lister(d, f);
   }
}
