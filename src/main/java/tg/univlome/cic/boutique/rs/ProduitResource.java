/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tg.univlome.cic.boutique.rs;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import tg.unilome.cic.boutique.service.ProduitService;
import tg.univlome.cic.boutique.entites.Produit;

/**
 *
 * @author GOUNTENI
 */
@Path("/produits")
public class ProduitResource {
 
    
    ProduitService monproduit;
        
    
    @POST
    @Path("/ajouter")
    public void ajouter(@QueryParam("c") Produit c){
        monproduit.ajouter(c);
    }
    
    @PATCH
    @Path("/modifier{c}")
    public void modifier(@PathParam("c") Produit c){
        monproduit.modifier(c);
    }
    
    @GET
    @Path("/trouver")
    public void trouver(@QueryParam("id") Integer id){
        monproduit.trouver(id);
    }
    
    
    @DELETE
    @Path("/delete{id}")
    public void delete(@PathParam("id") Integer id){
        monproduit.supprimer(id);
    }
    
    
    @DELETE
    @Path("/delete{c}")
    public void delete(@PathParam("c") Produit c){
        monproduit.supprimer(c);
    }
    
    
    @GET
    @Path("/lister")
    public void afficher(){
       monproduit.lister();
   }
    
  
    @GET
    @Path("/lister")
    public void afficher(@QueryParam("d") Integer d, @QueryParam("f") Integer f){
       monproduit.lister(d, f);
   }
}
