/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tg.univlome.cic.boutique.rs;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import tg.unilome.cic.boutique.service.ProduitAcheteService;
import tg.univlome.cic.boutique.entites.ProduitAchete;

/**
 *
 * @author GOUNTENI
 */
@Path("/produits/achate")
public class ProduitAcheteResource {
    
    
    
    ProduitAcheteService monproduitachete;
        
    
    @POST
    @Path("/ajouter")
    public void ajouter(@QueryParam("c") ProduitAchete c){
        monproduitachete.ajouter(c);
    }
    
    @PATCH
    @Path("/modifier{c}")
    public void modifier(@PathParam("c") ProduitAchete c){
        monproduitachete.modifier(c);
    }
    
    @DELETE
    @Path("/delete{c}")
    public void delete(@PathParam("c") ProduitAchete c){
        monproduitachete.supprimer(c);
    }
    
    
    @GET
    @Path("/lister")
    public void afficher(){
       monproduitachete.lister();
   }
    
  
    @GET
    @Path("/lister")
    public void afficher(@QueryParam("d") Integer d, @QueryParam("f") Integer f){
       monproduitachete.lister(d, f);
   }
}
