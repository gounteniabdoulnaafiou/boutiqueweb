/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tg.univlome.cic.boutique.rs;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import tg.univlome.cic.boutique.entites.Personne;
import tg.univlome.cic.boutique.service.PersonneService;

/**
 *
 * @author GOUNTENI
 */
@Path("/personnes")
public class PersonneResource {
    
    
    
    PersonneService mapersonne;
        
    
    @POST
    @Path("/ajouter")
    public void ajouter(@QueryParam("c") Personne c){
        mapersonne.ajouter(c);
    }
    
    @PATCH
    @Path("/modifier{c}")
    public void modifier(@PathParam("c") Personne c){
        mapersonne.modifier(c);
    }
    
    @GET
    @Path("/trouver")
    public void trouver(@QueryParam("id") Integer id){
        mapersonne.trouver(id);
    }
    
    
    @DELETE
    @Path("/delete{id}")
    public void delete(@PathParam("id") Integer id){
        mapersonne.supprimer(id);
    }
    
    
    @DELETE
    @Path("/delete{c}")
    public void delete(@PathParam("c") Personne c){
        mapersonne.supprimer(c);
    }
    
    
    @GET
    @Path("/lister")
    public void afficher(){
       mapersonne.lister();
   }
    
  
    @GET
    @Path("/lister")
    public void afficher(@QueryParam("d") Integer d, @QueryParam("f") Integer f){
       mapersonne.lister(d, f);
   }
}
