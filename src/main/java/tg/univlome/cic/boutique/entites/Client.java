/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tg.univlome.cic.boutique.entites;


/**
 *
 * @author GOUNTENI
 */
public class Client extends  Personne{
    
    private String carteVisa;

    public Client() {
    }

    public Client(String carteVisa) {
        this.carteVisa = carteVisa;
    }

    public String getCarteVisa() {
        return carteVisa;
    }

    public void setCarteVisa(String carteVisa) {
        this.carteVisa = carteVisa;
    }

    @Override
    public String toString() {
        return "Client{" + "carteVisa=" + carteVisa + '}';
    }
    
}
