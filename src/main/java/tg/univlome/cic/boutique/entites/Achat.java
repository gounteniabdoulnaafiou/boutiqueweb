/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tg.univlome.cic.boutique.entites;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;


/**
 *
 * @author GOUNTENI
 */
public class Achat implements Serializable{
    private long id;
    private double remise = 0.0;
    private LocalDate dateAchat;
    private List<ProduitAchete> produitachete = new LinkedList<>();
    private Employe employe;

    public Achat() {
    }

    public Achat(long id, LocalDate dateAchat, double remise,  List<ProduitAchete> produitachete, Employe employe) {
        this.id = id;
        this.dateAchat = dateAchat;
        this.produitachete = produitachete;
        this.employe = employe;
        this.remise = remise;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getRemise() {
        return remise;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }

    public LocalDate getDateAchat() {
        return dateAchat;
    }

    public void setDateAchat(LocalDate dateAchat) {
        this.dateAchat = dateAchat;
    }

    public List<ProduitAchete> getProduitachete() {
        return produitachete;
    }

    public void setProduitachete(List<ProduitAchete> produitachete) {
        this.produitachete = produitachete;
    }

    public Employe getEmploye() {
        return employe;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    public double getRemiseTotal(){
        double remisetotal = this.remise;
        for (ProduitAchete produitAchete : produitachete) {
            remisetotal += produitAchete.getRemise();
        }
        
        return remisetotal;
    }
    
    public double getPrixTotal(){
        double total = -this.remise;
        for (ProduitAchete produitAchete : produitachete) {
            total += produitAchete.getPrixTotal();
        }
        
        return total < 0 ? 0 : total;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Achat other = (Achat) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Achat{" + "id=" + id + ", remise=" + remise + ", dateAchat=" + dateAchat + ", produitachete=" + produitachete + ", employe=" + employe + '}';
    }
    
    
}
