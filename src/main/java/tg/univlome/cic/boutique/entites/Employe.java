/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tg.univlome.cic.boutique.entites;

import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author GOUNTENI
 */
public class Employe extends Personne{
    private String cnss;
    private LocalDate dateEmbauche;
    private List<Achat> achats;


    public String getCnss() {
        return cnss;
    }

    public void setCnss(String cnss) {
        this.cnss = cnss;
    }

    public LocalDate getDateEmbauche() {
        return dateEmbauche;
    }

    public void setDateEmbauche(LocalDate dateEmbauche) {
        this.dateEmbauche = dateEmbauche;
    }

    public List<Achat> getAchats() {
        return achats;
    }

    public void setAchats(List<Achat> achats) {
        this.achats = achats;
    }
    
    
    @Override
    public String toString() {
        return "Employe{" + "cnss=" + cnss + ", dateEmbauche=" + dateEmbauche + ", achats=" + achats + '}';
    }
     
}
