/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tg.univlome.cic.boutique.entites;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;


/**
 *
 * @author GOUNTENI
 */
public class Personne implements  Serializable {
    
    protected long id;
    protected String nom;
    protected String prenom;
    protected LocalDate dateNaissance;

    public Personne(long id, String nom, String prenom, LocalDate dateNaissance) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
    }
    
    public Personne(){
        
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Personne other = (Personne) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }



    @Override
    public String toString() {
        return "Personne{" + "id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", dateNaissance=" + dateNaissance + '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }
    
    public int getAge(){
      return this.getAge(LocalDate.now());
    //return LocalDate.now().getYear() - this.dateNaissance.getYear();
    }
    
    public int getAge(LocalDate ref){
        if(this.dateNaissance == null || ref == null)
        {
            throw new IllegalArgumentException("La date de naissance et la date de réference ne peuvent être null");
        }
        Period periode = Period.between(this.dateNaissance, ref);
        return periode.getYears() < 0 ? -periode.getYears() : periode.getYears();
    }
}
