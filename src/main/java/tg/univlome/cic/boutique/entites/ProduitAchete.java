/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tg.univlome.cic.boutique.entites;


/**
 *
 * @author GOUNTENI
 */
public class ProduitAchete {
    
    private int quantite;
    private double remise;
    Produit produit;
    private Produit achete;

    public ProduitAchete() {
        this.quantite = 1;
        this.remise = 0.0;
    }


    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public double getRemise() {
        return remise;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }

    public Produit getAchete() {
        return achete;
    }

    public void setAchete(Produit achete) {
        this.achete = achete;
    }


    public double getPrixTotal(){
        
        return this.quantite * this.produit.getPrixUnitaire() - this.remise;
                   
    }


    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + this.quantite;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProduitAchete other = (ProduitAchete) obj;
        if (this.quantite != other.quantite) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ProduitAchete{" + "quantite=" + quantite + ", remise=" + remise + ", produit=" + produit + ", achete=" + achete + '}';
    }
    
}
